import java.nio.file.Paths
import java.nio.file.Files
import static java.nio.file.StandardCopyOption.REPLACE_EXISTING

if (this.args.length < 3) {
  println "usage: groovy copy-unprocessed-files.groovy [input-directory] [file-unprocessed-filenames] [output-dir]"
  return 1
}

def inputDirectory = Paths.get(this.args[0])
def fileWithUnprocessedFilenames = Paths.get(this.args[1])
def outputDirectory = Paths.get(this.args[2])

if (!Files.isDirectory(inputDirectory)) {
  println "The input directory ${inputDirectory} does not exist!"
  return 1
}

if (!Files.exists(fileWithUnprocessedFilenames)) {
  println "Cannot find the file with unprocessed filenames: ${fileWithUnprocessedFilenames}"
  return 1
}

if (!Files.isDirectory(outputDirectory)) {
  println "The output directory ${outputDirectory} does not exist!"
  return 1
}

def unprocessedFiles = []
fileWithUnprocessedFilenames.toFile().readLines().each { line ->
  unprocessedFiles << Paths.get(line.trim())
}

unprocessedFiles.each { unprocessedFile ->
  def source = inputDirectory.resolve(unprocessedFile)
  def destination = outputDirectory.resolve(unprocessedFile)
  println "copying ${source} to ${destination}"

  Files.copy(source, destination, REPLACE_EXISTING)
  println "done"
}
