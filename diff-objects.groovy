@Grab("com.amazonaws:aws-java-sdk-s3:1.11.505")
import com.amazonaws.AmazonServiceException
import com.amazonaws.SdkClientException
import com.amazonaws.auth.profile.ProfileCredentialsProvider
import com.amazonaws.services.s3.AmazonS3
import com.amazonaws.services.s3.AmazonS3ClientBuilder
import com.amazonaws.services.s3.model.ObjectMetadata
import com.amazonaws.services.s3.model.PutObjectRequest

import java.io.File
import java.nio.file.Paths
import java.nio.file.Files
import java.security.MessageDigest

if (this.args.length < 1) {
  println "usage: groovy upload.groovy [input-directory]"
  return 1
}

def inputDirectory = Paths.get(this.args[0])

if (!Files.isDirectory(inputDirectory)) {
  println "The directory ${outputDirectory} does not exist!"
  return 1
}

def s3Client = AmazonS3ClientBuilder.standard()
        .withRegion('eu-west-1')
        .withCredentials(new ProfileCredentialsProvider())
        .build()

def bucketName = 'rts-dota2proscraper-jsons'

int totalEqual = 0
int totalNotEqual = 0
def notEqualFiles = []

inputDirectory.toFile().eachFileMatch(~/.*.dem.10.json/) { file ->

  def localMd5 = getContentMD5(file.bytes)
  def objectMetadata = s3Client.getObjectMetadata(bucketName, file.name)

  println "${file.name}: localMd5 = ${localMd5}, s3Etag = ${objectMetadata.getETag()}"

  if (localMd5 == objectMetadata.getETag()) {
    totalEqual++
  } else {
    totalNotEqual++
    notEqualFiles << file.name
  }
}

def getContentMD5(byte[] fileContent){
    MessageDigest.getInstance("MD5").digest(fileContent).encodeHex().toString()
}

println "totalEqual: ${totalEqual}, totalNotEqual: ${totalNotEqual}"
println "notEqualFiles: ${notEqualFiles}"
