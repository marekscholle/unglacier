@Grab("com.amazonaws:aws-java-sdk-s3:1.11.505")
import com.amazonaws.AmazonServiceException
import com.amazonaws.SdkClientException
import com.amazonaws.auth.profile.ProfileCredentialsProvider
import com.amazonaws.services.s3.AmazonS3
import com.amazonaws.services.s3.AmazonS3ClientBuilder
import com.amazonaws.services.s3.model.ObjectMetadata
import com.amazonaws.services.s3.model.PutObjectRequest

import java.io.File
import java.nio.file.Paths
import java.nio.file.Files

if (this.args.length < 1) {
  println "usage: groovy does-object-exist.groovy [input-directory]"
  return 1
}

def inputDirectory = Paths.get(this.args[0])

if (!Files.isDirectory(inputDirectory)) {
  println "The directory ${inputDirectory} does not exist!"
  return 1
}

def s3Client = AmazonS3ClientBuilder.standard()
        .withRegion('eu-west-1')
        .withCredentials(new ProfileCredentialsProvider())
        .build()

def bucketName = 'rts-dota2proscraper-jsons'

int totalExist = 0
int totalNotExist = 0
def notExisting = []

inputDirectory.toFile().eachFileMatch(~/.*.dem.10.json/) { file ->

  def exists = s3Client.doesObjectExist(bucketName, file.name)
  println "object ${file.name} exists: ${exists}"

  if (exists) {
    totalExist++
  } else {
    totalNotExist++
    notExisting << file.name
  }
}

println "totalExist: ${totalExist}, totalNotExist: ${totalNotExist}"
println "notExisting: ${notExisting}"
