@Grab("com.amazonaws:aws-java-sdk-s3:1.11.505")
import com.amazonaws.auth.profile.ProfileCredentialsProvider
import com.amazonaws.services.s3.AmazonS3ClientBuilder
import com.amazonaws.services.s3.model.S3Object
import com.amazonaws.services.s3.model.GetObjectRequest
import java.nio.file.Paths
import java.nio.file.Files

if (this.args.length < 2) {
  println "usage: groovy download.groovy [path-to-glacier-objects] [output-directory]"
  return 1
}

def path = this.args[0]
def outputDirectory = Paths.get(this.args[1])

if (!Files.isDirectory(outputDirectory)) {
  println "The directory ${outputDirectory} does not exist!"
  return 1
}

def restoredGlacierObjects = new File(path) as String[]
def keys = restoredGlacierObjects.collect { it.split(',')[1] }
keys.removeAt(0)

def s3Client = AmazonS3ClientBuilder.standard()
        .withCredentials(new ProfileCredentialsProvider())
        .withRegion('eu-west-1')
        .build()
def bucketName = 'rts-dota2proscraper-demos'

keys.each { key ->
  println "downloading S3 object with key '${key}'"
  S3Object s3Object = null
  try {
    s3Object = s3Client.getObject(new GetObjectRequest(bucketName, key))
    def outputPath = outputDirectory.resolve(key)
    println "saving S3 object as '$outputPath'"
    outputPath.withOutputStream { out ->
      out << s3Object.getObjectContent()
    }
  } finally {
    s3Object?.close()
  }
}
