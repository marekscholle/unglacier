import java.nio.file.Paths
import static java.nio.file.Files.move
import static java.nio.file.StandardCopyOption.REPLACE_EXISTING

if (this.args.length < 2) {
  println "usage: groovy find-files-to-be-processed.groovy [dir-with-jsons] [dir-with-dems]"
  return 1
}

def dirWithJsons = this.args[0]
def dirWithDems = this.args[1]

def processedFilesNames = [] as Set
new File(dirWithJsons).eachFileMatch(~/.*.dem.json/) {
  processedFilesNames << it.name.replace(".dem.json", ".dem")
}

def demFiles = []
new File(dirWithDems).eachFileMatch(~/.*.dem/) {
  demFiles << it
}

def processedCount = 0
def unprocessedCount = 0
demFiles.each {
  if (processedFilesNames.contains(it.name)) {
    println "File '$it' has already been processed, will be removed"
    processedCount++
    // it.delete()
  } else {
    unprocessedCount++
  }
}

println "total dem files: ${demFiles.size()}, processed: $processedCount, unprocessed: $unprocessedCount, processed + unprocessed: ${processedCount + unprocessedCount}"
println "total json files: ${processedFilesNames.size()}"
