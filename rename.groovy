import java.nio.file.Paths
import static java.nio.file.Files.move
import static java.nio.file.StandardCopyOption.REPLACE_EXISTING

if (this.args.length < 1) {
  println "usage: groovy rename.groovy [dir]"
  return 1
}

def dir = this.args[0]
def ext = '.dem.json'

new File(dir).eachFileMatch(~/.*${ext}/) {
  def source = it.toPath()
  def destination = Paths.get(dir, it.name.replace(ext, ".dem.10.json"))
  println "renaming '$source' => '$destination'"

  move(source, destination, REPLACE_EXISTING)
}
